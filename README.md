# Bathymetry data processing with Deeper Pro and RTK Gps
![](imgs/MarkVej_bathimetry_section.png)
## Installation

Install all the required dependecies with
```
pip install -r requirements.txt
```

## Execution

After installing the requirements, inside the project folder run:
```
jupyter lab
```

## Google Earth points

After running the script with the **jupyter** notebook, a new folder **kml**
is created. In here there are all the **kml** files that can be inported to
google earth.