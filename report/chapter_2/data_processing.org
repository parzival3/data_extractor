The second part of this report explains the processing of the data extracted by the \payload and the \sonar. Since during the fieldwork the \sonar was malfunctioning, it was necessary to swap the \sonar with its previous version the \sonarprev which does not enable the forwarding of NMEA messages. The only available connection to the \sonarprev is the mobile phone application. Therefore the data used in this section does not reflect the data logged from the \payload. Another downside of using the old sonar was the impossibility to determine the precise location of each data point received by the sonar.  To overcome the unforeseen failure of the sonar, instead of dragging it with the UAV, the sonar was dragged manually across each bridge in the first two site while tracking the corresponding GPS position with the RTK GNSS rover station. In the last site the data was collected using the Deeper Sonar Pro instead of the Deeper Chirp.
* Data Processing
The tools used in this operation are the web-based development environment Jupyter Lab based on Python 3 for the processing of the data. Google Earth was used for visualizing the respective GPS points and have visual feedback on the processed data. The Python libraries used are the following

- /utm/: library for converting utm coordinates to latitude / longitude and vice versa.
- /simplekml/: library for creating kml files.
- /geopy/: library for measuring geographical distance.
- /pandas/: library for managing and manipulating datasets.
- /matplotlib/: library for plotting data.
- /sklearn/: library for machine learn primitives.

This section is divided into two subsections, the first one explains how the two datasets are merged, and the second one explains how to project the points into the cross-section.
** Merging the datasets
 The data received from the \sonar and the \rtkstation is stored in two plain CSV files. The sonar dataset has 4 attributes which are : /longitude/, /latitude/, /depth in meters/, /timestamp/, while for the RTK there are 10 attributes: /Pt name/, /North/, /East/, /Elevation/, /HA/, /VA/, /SD/, /Time/, /Data/, /Code/. Since we are using the RTK for geolocalize the sonar data, the attribute /longitude/ and /latitude/ are not relevant from the data processing and can be removed from the sonar dataset. Like the sonar, the features that are not relevant from the RTK dataset are the /HA/, /VA/, /SD/ and /Code/ which are removed from the dataset before starting the processing. Generally speaking, to combine two datasets, there must be a common feature used to link the data present in one dataset to the data found in the other. The attribute that is common between the two sets is the /timestamp/, which is stored as a Unix timestamp with 13 digits in the sonar dataset while in the RTK is stored as /date/ and /time/ format. Since the two different timestamp formats, the first step for merging the two datasets was to convert the RTK data into the Unix timestamp and the sonar data to date/time. Since Python accepts only ten digits timestamp for the conversion between Unix timestamp to date/time, it is necessary to divide the sonar timestamp for 1e3 before converting it. After converting the two timestamp in both formats, it is possible to remove all the unnecessary observation present in the sonar dataset by filtering out all the observations that have a timestamp lower or higher than the first and last RTK collected on the bridges. After that, it is also possible to remove all the observations in the RTK set that are not relevant, this is done by filtering out the observation by /Pt name/. When all the points are filtered out, it is possible to plot them in a kml file with the =simplekml= library like in figure [[fig:rtkpointsgamle]]

 #+CAPTION: RTK points in MarkVej
 #+NAME: fig:rtkpointsgamle
 #+ATTR_LATEX: :width 0.8\linewidth
 #+ATTR_ORG: :width 50px
 [[~/Git/data_extractor/imgs/MarkVej_all_points.png]]


  After having narrowed down both dataset with only the necessary information, the data can be combined in two ways. The first one and the most simple is to use a simple comparison between the two timestamps. This means that for some RTK points, the bathymetry measure is empty because there is no corresponding sonar measure with the same timestamp. The second option is to classify each RTK points with one of the sonar bathymetry measures by using KNN. At the end of this process to new feature are added to the dataset of the RTK. One feature called /bathymetry/ which represents the sonar data assigned with the first method previously explained and the second one called /bathymetry_knn/ which uses the second method. Before proceeding with the cross-section projection, is good practice to visualize the data and see if the bathymetry data makes sense.

 #+CAPTION: Plotting of the bathymetry Markvej
 #+NAME: fig:sectionmakr
 #+ATTR_LaTeX: :width 0.7\linewidth
 [[/home/enrico/Git/data_extractor/plot/markvejapprox.svg]]

 #+CAPTION: Plotting of the bathymetry Gudenavej
 #+NAME: fig:sectionguden
 #+ATTR_LaTeX: :width 0.7\linewidth
[[/home/enrico/Git/data_extractor/plot/gudenvejapprox.svg]]


For the third site, Skibelundvej figure [[fig:sectionthird]], the plot show the double passage of the drone in the stream.

 #+CAPTION: Plotting of the bathymetry Skibelundvej
 #+NAME: fig:sectionthird
 #+ATTR_LaTeX: :width 0.7\linewidth
[[/home/enrico/Git/data_extractor/plot/third.svg]]


** Project to the cross section
 After slicing and merging the datasets with only the relevant data, it is time to project the bathymetry measures to the cross-section. Figure [[fig:markvej_section]]  shows the first and last RTK point taken on the bridge and the bathymetry section used for the projection.
 #+CAPTION: Corss section for MarkVej
 #+NAME: fig:markvej_section
 #+ATTR_LATEX: :width 0.8\linewidth
 #+ATTR_ORG: :width 50px
 [[~/Git/data_extractor/imgs/MarkVej_bathimetry_section_2.png]]

 To project the bathymetry data on the cross-section, the cross-section has to be divided into segments of 10cm each. To do so, we use the fundamental equation of the line
 \[
 \Delta_y = m \Delta_x
 \]
 Where we define $m$ as our increment or delta ($\Delta$), in this case, $m$ can be express as the ratio between $\Delta_y$, and $\Delta_x$.
 \[
 \Delta_y = y_f - y_i \quad,\quad \Delta_x = x_f - x_i
 \]
 As described in the figure [[fig:coordinates]], *North* is the $y$ axis and *East* is the $x$ axis.

 #+CAPTION: UTM coordinates
 #+NAME: fig:coordinates
 #+ATTR_LATEX: :width 0.8\linewidth
 #+ATTR_ORG: :width 50px
 [[~/Git/data_extractor/imgs/Coordinates.png]]

 With this informations, we can proceed to calculate $\Delta_y$ and $\Delta_x$, as shown below. Now that we know the value of $m$, we can proceed and calculate the increment for $x$ and $y$ for the next point. In order to not confuse the two different increment, we will call these two increments $\Delta_{x1}$ and $\Delta_{y1}$. From the formula for the distance between two points, we know that.
\[
 dx^2 = \Delta_{x1}^2 + \Delta_{y1}^2
 \]
 Where in this case $dx = 0.1 [m]$.
 And from the euqtion of the line:
 \[
 \Delta_{y1} = m \Delta_{x1}
 \]
 And if we plug this in the equation of the distance between two points we find that:
 \begin{align}
 dx^2 & = (m \Delta_{x1})^2 + \Delta_{x1}^2 \\
 dx^2 & = \Delta_{x1}^2 (m^2 + 1)
 \end{align}
 And finally:
 \[
 \Delta_{x1} = \sqrt{\frac{dx^2}{m^2 + 1}} \quad \quad \Delta_{y1} = \sqrt{dx^2 - \Delta_{x1}^2}
 \]

 The results are shown in figure \ref{fig:sections}
#+BEGIN_EXPORT latex
\begin{figure}[h]
\begin{subfigure}{0.5\textwidth}
\includegraphics[width=0.9\linewidth, height=5cm]{/home/enrico/Git/data_extractor/imgs/Sections.png}
\caption{Markvej}
\label{fig:subim1}
\end{subfigure}
\begin{subfigure}{0.5\textwidth}
\includegraphics[width=0.9\linewidth, height=5cm]{/home/enrico/Git/data_extractor/imgs/Gunvej_sections.png}
\caption{Gudenavej}
\label{fig:subim2}
\end{subfigure}
\caption{Plotting of poles ans points along the crossections}
\label{fig:sections}
\end{figure}
#+END_EXPORT


 After finding all the points composing the cross-section line, KNN is used to classify and project each of the RTK to it. The final plots are displayed in figure [[fig:markvej_proj]] and [[fig:gudenavej_proj]].
 #+CAPTION: Markvej cross section plot
 #+NAME: fig:markvej_proj
 #+ATTR_LATEX: :width \linewidth
 #+ATTR_ORG: :width 50px
 [[~/Git/data_extractor/plot/markvej_projection.svg]]

 #+CAPTION: Gudenavej cross section plot
 #+NAME: fig:gudenavej_proj
 #+ATTR_LATEX: :width \linewidth
 #+ATTR_ORG: :width 50px
 [[~/Git/data_extractor/plot/gudenvej_projection.svg]]
