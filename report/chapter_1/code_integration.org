** \sonar
  The \sonar is one of the most used fish finders in the fishing market, and its relatively low price, its limited weight and dimension make it for one of the best sonar for this specific task \cite{DeeperWebsite}. Figure [[fig:deepersonar]], illustrates the \sonar.
#+CAPTION: \sonar \cite{DeeperWebsite}
 #+NAME: fig:deepersonar
 #+ATTR_LATEX: :width 0.2\linewidth
 #+ATTR_ORG: :width 25px
 [[~/Git/data_extractor/report/imgs/chapter1/deeperSonar.png]]

The \sonar uses the \chirp technology explained in [[\chirp Compressed High Intensity Radar Pulse]] to increase the signal-to-noise ratio and have a better resolution on the data of the seabed. The regular communication between the sonar and the user revolves around the Android/IOS application. When the sonar is in contact with water, it automatically powers on and starts a Wi-Fi hotspot that can be used by the respective phone application to retrieve the data extracted by the sonar and configure the device. The sonar has three different operation frequencies, which are listed in table [[tab:deeperspecs]]. Each one of these frequencies provides a different precision which is listed in table [[table:frequencyspecs]]. Since the working frequency controls the accuracy of the sensor, the narrowest frequency was used during the fieldwork.

 #+NAME: tab:deeperspecs
 #+CAPTION: \sonar specifications \cite{DeeperWebsite}
 |-----------------------+--------------------------------|
 | Sonar Frequency 1     | Narrow 675 kHz (7°)            |
 | Sonar Frequency 2     | Medium 290 kHz (16°)           |
 | Sonar Frequency 3     | Wide   100 kHz (47°)           |
 | Depth Range           | 15 cm / 100 m                  |
 | Sonar Scan Rate       | Up to 15 scans per second      |
 | Operating Temperature | -20°C to 40°C                  |
 | Operating Time        | Up to 6 hours                  |
 | Connection Type       | Wi-Fi                          |
 | Connection Range      | Stable connection up to  100 m |
 |-----------------------+--------------------------------|

 Other than using the \chirp technology, this version of the sonar enables the developers to interact with it through NMEA messages.

 #+NAME: table:frequencyspecs
 #+ATTR_LATEX: table align=lccc
 |            | Narrow 675 kHz | Medium 290 kHz | Wide 100 kHz |
 |------------+----------------+----------------+--------------|
 | Separation | 1 cm           | 2.4 cm         | 2.4 cm       |


 #+CAPTION: \sonar specifications \cite{DeeperWebsite}
 #+NAME: fig:deepersonarspecs
 #+ATTR_LATEX: :width 0.5\linewidth
 #+ATTR_ORG: :width 50px
 [[~/Git/data_extractor/report/imgs/chapter1/sonarSpecs.png]]

** NMEA protocol
 NMEA 0183 or more commonly NMEA, is a communication standard used mainly in marine and GPS applications. The standard is developed and maintained by the National Marine Electronics \cite{NMEANational}. The protocol revolves around two types of sources, the =talker= and the =listner=. In this protocol, the =talker= can only send data, while the =listener= can only receive it.

*** NMEA messages
 Each of the NMEA messages has the following structure \cite{NMEAGen}:
    #+BEGIN_SRC
 $PREFIX,data1,data2 ... dataN-1,dataN*CHECKSUM
    #+END_SRC

 The sentence always starts with $ and always ends with =CR LF= or in ASCII ~<0D><0A>~. Each sentence is at most 80 characters long.

**** *Prefix*:
The prefix is the first part of the string, which is used to identify the category of the message, for example, autopilot, GPS device, speed control, direction control, etc. If using a GPS device, the prefix is GP followed by the type of the phrase. All sentences are identified with 3 letters (e.g. RMC, RMB, etc.)

**** *Checksum*:
 The Checksum is calculated excluding the beginning of the string and the =*= character. The algorithm used is the exclusive OR 8bit, composing the result in 2 letters or numbers. The most significant of the two will be sent first.

*** \sonar NMEA messages
 The \sonar supports a variety of NMEA messages, which include GPS messages, DBT (depth below transducer messages) and MTW (Mean Temperature of the Water). In this specific application, the only relevant message is the =DBT= which has a form of:
 #+BEGIN_SRC
 $SDDBT,D_FEET,f,D_METRES,M,D_FATHOMS,F*hh<0D><0A>
 #+END_SRC

To start receiving the NMEA messages, the sonar has to be turned on and the =client= connected to its Wi-Fi hotspot. The =client= can be any device capable of sending UPD packets. In this particular case the =client= is the \payload used in the UAS. After the client has established a connection to the hotspot, a UDP socket is open at ~192.168.10.1:10110~ and the following NMEA message is sent:

 #+BEGIN_SRC
 $DEEP230,1*38<0D><0A>
 #+END_SRC
After that, the sonar will start forwarding NMEA packets to the same IP address. This means that at the beginning, the =client= application acts as a =talker= and then, switches role and act as a =listener=. Other than enabling and disabling the forwarding of messages, the sonar supports three additional commands which are used to set the frequency of the sonar: \cite{NMEADip}

  | Frequency                      | Command                  |
  |--------------------------------+--------------------------|
  | Wide angle 90 kHz – 115 kHz    | ~$DEEP231,4*3C <0D><0A>~ |
  | Medium angle 270 kHz – 310 kHz | ~$DEEP231,5*3D <0D><0A>~ |
  | Narrow angle 635 kHz – 715 kHz | ~$DEEP231,6*3E <0D><0A>~ |
