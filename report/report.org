#+OPTIONS: title:nil toc:nil
#+TITLE: Development of an embedded system solution for UAS river bathymetry
#+LATEX_CLASS: article
#+LATEX_HEADER: \newcommand{\payload}{TRL9 payload }
#+LATEX_HEADER: \newcommand{\sonar}{\textit{Deeper chirp smart sonar \texttrademark}}
#+LATEX_HEADER: \newcommand{\sonarprev}{\textit{Deeper chirp}}
#+LATEX_HEADER: \newcommand{\chirp}{\textit{CHIRP }}
#+LATEX_HEADER: \newcommand{\beagle}{BeagleBone Black Wireless }
#+LATEX_HEADER: \newcommand{\matrice}{DJI Matrice 600 }
#+LATEX_HEADER: \newcommand{\rtkstation}{RTK GNSS rover station }
#+LATEX_HEADER: \usepackage[inkscapeformat=pdf,inkscape=forced, inkscapearea=page]{svg}
#+LATEX_HEADER: \usepackage{subcaption}
#+LATEX_HEADER: \usepackage[toc,page]{appendix}
#+LATEX_HEADER: \usepackage{pdfpages}
#+LATEX_HEADER: \hypersetup{pageanchor=false}

#+BEGIN_EXPORT latex
\begin{titlepage}
\author{\begin{tabular}{ll}
                     \\
                     Enrico Tolotto & s190057 \\
                     \\
\hspace{26mm}
\end{tabular}}
\title{
\includegraphics[height=3.5cm]{imgs/dtuLogo.png}\\
\normalfont\normalsize
\vspace{1cm}
\textsc{\huge Danmarks Tekniske Universitet} \\ [25pt]
\noindent\rule{\linewidth}{2pt}\\[0.3cm]
\huge Development of an embedded system solution for UAS river bathymetry\\ [0.3cm]
\noindent\rule{\linewidth}{2pt}\\[0.1cm]
\date{\today}
}
\maketitle
\thispagestyle{empty}
\end{titlepage}
\pagebreak
\tableofcontents
\pagebreak
#+END_EXPORT

** Introduction
   #+INCLUDE: "chapter_1/introduction.org"
** Study site
   #+INCLUDE: "chapter_1/study_site.org"
** Payload
   #+INCLUDE: "chapter_1/payload.org"
** Payload application
   #+INCLUDE: "chapter_1/code.org"
** Data Processing
   #+INCLUDE: "chapter_2/data_processing.org"
** Future Development
   #+INCLUDE: "chapter_1/future_development.org"
** Conclusion
   #+INCLUDE: "chapter_2/conclusion.org"
\pagebreak

\bibliographystyle{unsrt}
\bibliography{citation}
\pagebreak

#+LATEX: \begin{appendices}
   \section{\chirp Compressed High Intensity Radar Pulse}
   \label{chirp}
   #+INCLUDE: "chapter_1/appendix.org"
   \section{Python Notebooks}
   This appendix includes the Python Notebooks used to process the data. Since for the two sites Markvej and Gudenavej the processing is similar, only the notebook for Markvej is included
   \subsection{Processing of Markvej Cross Section}
   \includepdf[pages=-]{./MarkVej.pdf}
   \subsection{Processsng of Svostrup Bathymetry Data}
   \includepdf[pages=-]{./Svostrup.pdf}
#+LATEX: \end{appendices}

** Personal overview                                               :noexport:
[[./imgs/other/chapter_2_overview.png]]
